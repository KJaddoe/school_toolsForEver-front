(function() { 'use strict';

    angular
        .module('app.menu')
        .directive('mainMenu', mainMenu);

    function mainMenu() {

        return {
            restrict: 'EA',
            replace: true,
            controller: 'menuCtrl',
            controllerAs: 'mc',
            templateUrl: '/html/menu.html',
            link: postLink,
        };

        function postLink(scope, element, attr) {
        }

    }

})();
