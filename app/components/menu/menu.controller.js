(function() { 'use strict';

    angular
        .module('app.menu')
        .controller('menuCtrl', menuCtrl);

    menuCtrl.$inject = ['$scope', '$state', '$mdSidenav', 'MenuLinksService'];

    function menuCtrl($scope, $state, $mdSidenav, menu) {

        /* jshint validthis:true */
        var vm = this;

        vm.toggleMenu = buildToggler('left');

        vm.menu = menu;

        function buildToggler(componentId) {
            return function() {
                $mdSidenav(componentId).toggle();
            };
        }

    }

})();
