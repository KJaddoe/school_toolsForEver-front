(function() { 'use strict';

    angular
        .module('app.employee')
        .directive('employee', employee);

    function employee() {

        return {
            restrict: 'EA',
            replace: true,
            controller: 'employeeCtrl',
            controllerAs: 'vm',
            templateUrl: '/html/employee.directive.html',
            link: postLink,
        };

        function postLink(scope, element, attr) {
        }

    }

})();
