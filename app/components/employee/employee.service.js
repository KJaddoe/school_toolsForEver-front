(function() { 'use strict';

    angular
        .module('app.employee')
        .factory('employeeService', employeeService);

    employeeService.$inject = ['$resource', 'config'];

    function employeeService($resource, config) {

        return {
            home: {
                title: 'home',
                state: 'front',
            },
            contact: {
                title: 'contact',
                state: 'contact',
            },
            login: {
                title: 'Login',
                state: 'front',
            },
        };

        /*return $resource(config.get('apiHost') + '/api/menu_items/:id?_format=:format',
            {},
            {
                getSiteMenu: {
                    method: 'GET',
                    params: {
                        id: 'site-menu',
                        format: 'hal_json',
                    },
                },
                getSocialMenu: {
                    method: 'GET',
                    params: {
                        id: 'social-menu',
                        format: 'hal_json',
                    },
                },
            }
        );*/
    }

})();
