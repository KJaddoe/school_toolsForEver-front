(function() { 'use strict';

    angular
        .module('app.footer')
        .directive('siteFooter', siteFooter);

    function siteFooter() {

        return {
            restrict: 'EA',
            replace: true,
            controller: 'footerCtrl',
            controllerAs: 'fc',
            templateUrl: '/html/footer.html',
            link: postLink,
        };

        function postLink(scope, element, attr) {
        }

    }

})();
