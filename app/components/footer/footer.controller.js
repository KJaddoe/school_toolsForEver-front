(function() { 'use strict';

    angular
        .module('app.footer')
        .controller('footerCtrl', footerCtrl);

    footerCtrl.$inject = ['$scope'];

    function footerCtrl($scope) {

        /* jshint validthis:true */
        var vm = this;

        vm.currentYear = new Date().getFullYear();

    }

})();
