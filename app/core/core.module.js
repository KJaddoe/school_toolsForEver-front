(function() { 'use strict';

    angular.module('app.core', [

        /**
         * Third party libs
         */
        'ui.router',
        'ngResource',
        'ngMaterial',
        'ngMdIcons',

        /**
         * site modules
         */
        'site.seo',

        /**
         * App modules
         */
        'app.menu',
        'app.footer',
        'app.employee',

    ])
    .run(run);

    run.$inject = ['$rootScope', '$state', 'seo', 'config'];

    function run($rootScope, $state, seo, config) {

        $rootScope.$state = $state;
        $rootScope.config = config;
        $rootScope.seo = seo;

        //$rootScope.$on('$stateChangeSuccess', onStateChangeSuccess);

        //function onStateChangeSuccess() {
            //console.log($rootScope);
        //}

    }

})();
