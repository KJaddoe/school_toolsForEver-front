(function() { 'use strict';

    angular
        .module('app.core')
        .config(config);

    config.$inject = ['$locationProvider', '$stateProvider', '$urlRouterProvider', '$windowProvider'];

    function config($locationProvider, $stateProvider, $urlRouterProvider, $windowProvider) {

        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');
        $urlRouterProvider.otherwise('/');


        $stateProvider
            .state('page', {
                url: '/',
                abstract: true,
                resolve: {
                    isAuthenticated: [function() {
                        return false;
                    }],
                },
                views: {
                    page : {
                        templateUrl: '/html/page.html',
                        controller: 'CoreCtrl',
                    },
                },
            })
            .state('employee', {
                url: 'employee',
                parent: 'page',
                resolve: {
                    seo: ['seo', function(seo) {
                        seo.set('pageTitle', 'employee');

                        return seo;
                    }],
                },
                views: {
                    'content' : {
                        templateUrl: '/html/employee.html',
                    },
                },
            })
            .state('contact', {
                url: 'contact',
                parent: 'page',
                resolve: {
                    seo: ['seo', function(seo) {
                        seo.set('pageTitle', 'Contact');

                        return seo;
                    }],
                },
                views: {
                    'content' : {
                        templateUrl: '/html/contact.html',
                    },
                },
            })
            .state('front', {
                url: '',
                parent: 'page',
                resolve: {
                    seo: ['seo', function(seo) {
                        seo.set('pageTitle', 'Home');

                        return seo;
                    }],
                },
                views: {
                    'content' : {
                        templateUrl: '/html/front.html',
                    },
                },
            });
    }

})();
