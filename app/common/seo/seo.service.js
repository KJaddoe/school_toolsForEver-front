(function() { 'use strict';

    angular
        .module('site.seo')
        .factory('seo', seo);

    seo.$inject = ['$log'];

    function seo($log) {

        /**
         * TODO: Allow for setting default values, prefixes and suffixes in configuration
         */

        return {
            get: get,
            set: set,
        };

        function get(key) {

            /* jshint validthis:true */
            var _this = this;

            if(!key) {
                return _this;
            } else if(this[key]) {
                return _this[key];
            } else {
                $log.error('SEO: please pass existing property name');
                return _this;
            }

        }
        function set(key, value) {

            /* jshint validthis:true */
            var _this = this;

            if(key && value) {

                _this[key] = value;

                return _this[key];

            } else {
                $log.error('SEO: please pass a key and value');
                return _this;
            }

        }



    }

})();
