(function() { 'use strict';

    angular.module('app', [

        /**
         * Application wide access
         */
        'app.core',

    ]);

})();
